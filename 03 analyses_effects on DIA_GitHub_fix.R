


# Tile ---------------------------------------------------------------

# v_355 ist die variable, die angibt, welches video als letztes gesehen wrude

# TL_LAB_END_OTHER_LASTVID
# 1 = positiv
# 
# 2 = negativ

## dann nur aus set 1 die leute nehmen und dann 

# daten und packages -------------------------------------------------

library(xlsx)
library(horst)
library(purrr)


data <- read.csv("/Users/kaihorstmann/Dropbox/01Uni/01Projekte/00promotion_projects/experiment/analysis/data preparation/exp_data/01prep_data/data.valid.36ids.07222016.csv")


codebook <- read.xlsx("/Users/kaihorstmann/Dropbox/01Uni/01Projekte/00promotion_projects/experiment/analysis/Codebook_Experiment_v1.1.xlsx", 1, stringsAsFactors=FALSE)

setwd("~/Dropbox/01Uni/01Projekte/00promotion_projects/experiment/presentations/experiment_EP_v1.0_Rmd")

# umkodieren ---------------------------------------------------------

data[data == -77] <- NA
data[data == 0] <- NA

# from 7-12
# recode
to.recode <- na.omit(codebook[codebook$Scale == "7 bis 12", "new.varname"])

# recode
data[to.recode] <- data[to.recode] - 6 
# check
psych::describe(data[to.recode])

# reversecode
to.reversecode <- na.omit(codebook[codebook$Coding == "-", "new.varname"])
to.reversecode

data[to.reversecode] <- (data[to.reversecode] - 7)*-1
psych::describe(data[to.reversecode])


# select relevant variables ------------------------------------------


getScores <- function(data, pattern, fun){
  # function returns the fun of the pattern
  vars <- grep(x = names(data), pattern = pattern, value = TRUE)
  stems <- unique(gsub("_[0-9]+$" , "", vars))
  outs <- purrr::map(stems, ~ fun(data[grep(x = names(data), . , value = TRUE)], na.rm = TRUE))
  names(outs) <- stems
  return(as.data.frame(outs))
}

names(data)
dia.lab.data <- getScores(data, pattern = "LAB+.+DIA", rowMeans)
dia.lab.data <- cbind(dia.lab.data, data["TL_LAB_END_OTHER_LASTVID"])



# function | select sets ---------------------------------------------

select.sets <- function(data, var){
  neg.set2.ll <- data$TL_LAB_END_OTHER_LASTVID == 2 # all values that were positive on set 1
  pos.set1.ll <- data$TL_LAB_END_OTHER_LASTVID == 2 # all values that were positive on set 1
  pos.set2.ll <- data$TL_LAB_END_OTHER_LASTVID == 1 # all values that were positive on set 2
  neg.set1.ll <- data$TL_LAB_END_OTHER_LASTVID == 1 # all values that were positive on set 2
  
  # make set 2 variable
  var.set1 <- var
  var.set2 <- gsub("1", "2", var)
  
  # new.var.set1 <- vector(length=length(pos.set2.ll))
  new.var.set1 <- rep(NA, length(pos.set2.ll))
  new.var.set2 <- rep(NA, length(pos.set2.ll))
  
  # from all positive inductions
  pos.set1.ll[is.na(pos.set1.ll)] <- FALSE
  new.var.set1[pos.set1.ll] <- data[pos.set1.ll, var.set1]
  pos.set2.ll[is.na(pos.set2.ll)] <- FALSE
  new.var.set1[pos.set2.ll] <- data[pos.set2.ll, var.set2]
  
  # from all negative inductions
  neg.set1.ll[is.na(neg.set1.ll)] <- FALSE
  new.var.set2[neg.set1.ll] <- data[neg.set1.ll, var.set1]
  neg.set2.ll[is.na(neg.set2.ll)] <- FALSE
  new.var.set2[neg.set2.ll] <- data[neg.set2.ll, var.set2]
  
  df <- data.frame(cbind(new.var.set1, new.var.set2))
  colnames(df) <- c(paste0("SC_", var.set1), paste0("SC_", var.set2))
  df
}




# preparation of BFI data --------------------------------------------

# set 1:
bfi.online.scores <- getScores(data, pattern = "VP_ON+.+BFI", rowMeans)

## need to be sorted
bfi.exp.scores.tl.1 <- getScores(data, pattern = "TL_LAB+.+SET1+.+BFI", rowMeans)
bfi.exp.scores.kf.1 <- getScores(data, pattern = "KON_LAB+.+SET1+.+BFI", rowMeans)

# set 2:
bfi.online.scores <- getScores(data, pattern = "VP_ON+.+BFI", rowMeans) # ok

# need to be sorted
bfi.exp.scores.tl.2 <- getScores(data, pattern = "TL_LAB+.+SET2+.+BFI", rowMeans)
bfi.exp.scores.kf.2 <- getScores(data, pattern = "KON_LAB+.+SET2+.+BFI", rowMeans)

# select sets
raw.data.tl <- cbind(bfi.exp.scores.tl.1, bfi.exp.scores.tl.2, data["TL_LAB_END_OTHER_LASTVID"])
to.sort <- grep(x = names(raw.data.tl), pattern = "SET1", value = TRUE)
tl.data.sorted <- data.frame(purrr::map(to.sort, ~ select.sets(raw.data.tl, . )))

raw.data.kf <- cbind(bfi.exp.scores.kf.1, bfi.exp.scores.kf.2, data["TL_LAB_END_OTHER_LASTVID"])
to.sort <- grep(x = names(raw.data.kf), pattern = "SET1", value = TRUE)
kf.data.sorted <- data.frame(purrr::map(to.sort, ~ select.sets(raw.data.kf, . )))

# final bfi data -----------------------------------------------------

full.bfi.scores <- cbind(kf.data.sorted, tl.data.sorted, bfi.online.scores)
bigfive <- unique(stringr::str_sub(names(full.bfi.scores), start= -2)) # extract names


# correlation of BFIK and BFI for four raters ------------------------

cors.list <- list()
bf.list <- list()

for ( i in 1:5) {
  # select big five domain
  bf <- bigfive[i]
  vp.var <- grep(x = names(full.bfi.scores), pattern = paste0("VP_+.+", bf), value = TRUE)
  other.var <- grep(x = names(full.bfi.scores), pattern = paste0("SC_+.+", bf), value = TRUE)
  cors.bf <- purrr::map(other.var, ~ cor(full.bfi.scores[, .], full.bfi.scores[, vp.var],
                                         use = "pairwise.complete.obs"))
  cors.bf <- unlist(cors.bf)
  cors.list[[i]] <- cors.bf
}

cors.df <- cors.list[[1]]

for (i in 2:5){ cors.df <- rbind(cors.df, cors.list[[i]]) }
rownames(cors.df) <- c("Extraversion", "Agreeableness", "Conscientiousness", "Neuroticism", "Openness")
cors.df


# 1.1, 1.2 und 5.1 are the only significant correlations.


# plot correlations BFI self/other -----------------------------------

png("99gr_BFI_selfOther_cors.png", width = 710, height=480)
cors.online.bfi <- cors.df

gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}

cols <- rev(gg_color_hue(4))

op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

plot(x = 1:5, bty = "n", 
     ylim = c(-.80, .80),
     xaxt= "n",
     ylab = "correlation",
     xlab = "Big Five",
     type = "n")
abline(h = 0, col = "grey80", lty = 2)

points(cors.online.bfi[,1], col = cols[1], type = "b", pch = 15)
points(cors.online.bfi[,2], col = cols[2], type = "b", pch = 15)
points(cors.online.bfi[,3], col = cols[3], type = "b", pch = 16)
points(cors.online.bfi[,4], col = cols[4], type = "b", pch = 16)

legend(3.4, -0.3, legend = c("positive - confederate", "negative - confederate", "positive - research assistant", "negative - research assistant"), 
       col = cols[1:4],
       pch = c(15, 15, 16, 16),
       bty = "n",
       cex = 1,
       text.width = 0.5)

axis(1, at = 1:5, labels = c("Extraversion", "Agreeableness", "Conscientiousness", "Neuroticism", "Openness"))

dev.off()


# Validity | Manipulation Check --------------------------------------

panas.lab <- getScores(data, pattern = "VP+.+LAB+.+PANAS+.", rowMeans)
panas.lab <- cbind(panas.lab, data["TL_LAB_END_OTHER_LASTVID"])
head(panas.lab)

## set 1 ist immer positiv, set 2 immer negativ


png("99gr_manip_check.png", width=711, height=400)
#  847 × 401 old siize

op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

boxplot(panas.lab$VP_LAB_SET1_PERS_PANAS_PA, panas.lab$VP_LAB_SET2_PERS_PANAS_PA,
        panas.lab$VP_LAB_SET1_PERS_PANAS_NA, panas.lab$VP_LAB_SET2_PERS_PANAS_NA,
        col = c(rep(cols[2], 2), rep(cols[4], 2)) , 
        at = c(1, 2, 3.5, 4.5),
        bty = "n",
        main = "",
        xlab = "",
        axes = FALSE, ylim = c(1, 7))

axis(1, at = c(1, 2, 3.5, 4.5), labels = c("PA - positive mood", "PA - negative mood", "NA - positive mood", "NA - negative mood"), line = 1)
axis(2)

pa.tetest <- t.test(panas.lab$VP_LAB_SET1_PERS_PANAS_PA, panas.lab$VP_LAB_SET2_PERS_PANAS_PA,
                    paired = TRUE)
na.tetest <- t.test(panas.lab$VP_LAB_SET1_PERS_PANAS_NA, panas.lab$VP_LAB_SET2_PERS_PANAS_NA,
                    paired = TRUE)

arrows(x0 = 1, y0 = 5.5, x1 = 1, y1 = 6, angle=0)
arrows(x0 = 2, y0 = 5.5, x1 = 2, y1 = 6, angle=0)
arrows(x0 = 1, y0 = 6, x1 = 2, y1 = 6, angle=0)

arrows(x0 = 3.5, y0 = 5.5, x1 = 3.5, y1 = 6, angle=0)
arrows(x0 = 4.5, y0 = 5.5, x1 = 4.5, y1 = 6, angle=0)
arrows(x0 = 3.5, y0 = 6, x1 = 4.5, y1 = 6, angle=0)

# install.packages("compute.es")
library(compute.es)
tes(pa.tetest$statistic, n.1 = 36, n.2 = 36)
tes(na.tetest$statistic, n.1 = 36, n.2 = 36)

# tStats(pa.tetest, out.text="plot")
mtext(bquote(paste(italic("t"), " = ", 7.53, "(", 35, "), ", italic("p"), 
                   " = ", "< 0.001, ", italic("d"), " = 1.77")),
      at = c(1.5,7))

# tStats(na.tetest, out.text="plot")

mtext(bquote(paste(italic("t"), " = ", -9.23, "(", 35, "), ", italic("p"), 
                   " = ", "< 0.001, ", italic("d"), " = -2.77")),
      at = c(4,7))


dev.off()

# situational perception ---------------------------------------------

# how were the two sets perceived? How was the correlation between situational perception set 1 and set2?
# How is the correlation between the perception of both tl and kf?

# 1. compute the correlation between the average situational perception and the perception of the experiment (set 1 and set 2) (the confederate has not chosen the situation, i. e. this should only be person-effects.
#2. compute the average correlation of participant situational perception and kf/tl situational perception, i. e. only the perception of the lab-situation


# DIAMONDS, mean changes across sets ---------------------------------

set1dias <- grep(x = names(dia.lab.data), pattern = "VP_LAB_SET1+.+DIA", value = TRUE)
set2dias <- gsub("1", "2", set1dias)

diasetmeans <- list()
for (i in 1:8) {
  diasetmeans[[i]] <- cbind(dia.lab.data[set1dias[i]], dia.lab.data[set2dias[i]])
}
diasetmeans <- data.frame(diasetmeans)


# plot diamonds mean change across situation -------------------------


png("99gr_dia_meanChange_acrossSets.png", height = 480, width = 711)
op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

cols.dia <- map(gg_color_hue(8), ~ rep( . , 2))

boxplot(diasetmeans, 
        bty = "n",
        axes = FALSE,
        col = unlist(cols.dia))

# y-axis
axis(2)
# x-axis
axis(1, at = c(1:16), labels = rep(c("pos.", "neg."), 8))
axis(1, at = seq(from = 1.5, to = 1.5*11, 2), labels = unlist(strsplit("DIAMONDS", "")),
     line = 1, tick = FALSE)

dev.off()


# Effect of mood on DIAMONDS | Correlations across situations --------

set1dias <- grep(x = names(dia.lab.data), pattern = "VP_LAB_SET1+.+DIA", value = TRUE)
set2dias <- gsub("1", "2", set1dias)

diasetcors <- list()
for (i in 1:8) {
  diasetcors[[i]] <- cor(dia.lab.data[,set1dias[i]],dia.lab.data[,set2dias[i]])
}

diasettvalues <- list()
for (i in 1:8) {
  diasettvalues[[i]] <- t.test(dia.lab.data[,set1dias[i]],dia.lab.data[,set2dias[i]], paired = TRUE)
}



# DIAMONDS changes on the item level ---------------------------------

set1dias <- grep(x = names(data), pattern = "VP_LAB_SET1+.+DIA", value = TRUE)
set2dias <- gsub("SET1", "SET2", set1dias)

diavartvalues <- list()
for (i in seq_along(set1dias)) {
  diavartvalues[[i]] <- t.test(data[,set1dias[i]],data[,set2dias[i]], paired=TRUE)
}

## no changes on the item level


# plot ---------------------------------------------------------------


png("99gr_dia_cors_set1and2.png", height=480, width = 710)

op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

plot(unlist(diasetcors),
     ylim = c(0, 1),
     xaxt= "n",
     ylab = "correlation",
     xlab = "DIAMONDS",
     type = "b",
     bty = "n",
     col = cols[2], 
     main = "Bivariate Correlations of Situational Perception across sets")

axis(1, at = 1:8, labels = unlist(strsplit("DIAMONDS", "")))
mtext(bquote(paste("all correlations are significant at ", italic("p"), " = .05")), 
      side = 1, line = 2.2, at = 7)

dev.off()



# BFI correlations of Ratings across sets ----------------------------

# correlate rating from set 1 with rating from set 2

varstocor1 <- grep(x = names(full.bfi.scores), pattern = "SC+.+SET1", value= TRUE)

var1 <- varstocor1[1]
correlate.bfis <- function(var1){
  varstocor2 <- gsub("1", "2", var1)
  cor(full.bfi.scores[var1], full.bfi.scores[varstocor2], use = "pairwise.complete.obs")
}
bfi.cors.list <- purrr::map(varstocor1, correlate.bfis)
bfi.cors.vec <- unlist(bfi.cors.list)

bfi.cors.df <- rbind(bfi.cors.vec[1:5], bfi.cors.vec[6:10])

colnames(bfi.cors.df) <- c("Extraversion", "Agreeableness", "Conscientiousness", "Neuroticism", "Openness")
rownames(bfi.cors.df) <- c("Confederate", "Research Assistant")


# plot correlations between sets bfi ---------------------------------


png("99gr_BFI_corrAcrossSet.png", height = 480, width = 711)

op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

plot(x = 1:5, bty = "n", 
     ylim = c(0, 1),
     xaxt= "n",
     ylab = "correlation",
     xlab = "Big Five",
     type = "n",
     main = "Bivariate Correlations of BFI-ratings across Sets",
     cex.axis = 1,
     cex.lab = 1.3)
# abline(h = 0, col = "grey80", lty = 2)

points(bfi.cors.df[1,], col = cols[2], type = "b", pch = 15)
points(bfi.cors.df[2,], col = cols[3], type = "b", pch = 16)

legend(3, 0.4, legend = c("behavior rating of Research Assistant", "behavior rating of Confederate"),
       col = cols[2:3],
       pch = rep(c(15, 16), 2),
       bty = "n",
       cex = 1,
       text.width = 0.5)
axis(1, at = 1:5, labels = c("Extraversion", "Agreeableness", "Conscientiousness", "Neuroticism", "Openness"), cex.axis = 1.3)

dev.off()

# change of other rated behavior over two sets -----------------------

png("99gr_BFI_changes.png", width = 710, height=480)

op <- par(oma = c(5,4,0,0) + 0.1,
          mar = c(0,0,1,1) + 0.1)

cols.bf <- rev(gg_color_hue(8))
cols.bf <- sort(rep(cols.bf[4:8], 2))

# select only variables of confederate:
bfik.lab <- full.bfi.scores[grep(x = names(full.bfi.scores), pattern = "SC+.+KON", value= TRUE)]
ncol(bfik.lab)
# bigfive <- stringr::str_sub(names(bfik.lab), start= -2)

b <- boxplot(bfik.lab, 
             bty = "n",
             col = cols.bf,
             axes = FALSE)

# y-axis
axis(2)
# x-axis
axis(1, at = c(1:10), labels = rep(c("positive", "negative"), 5))
bf.labels <- c("Extraversion", "Agreeableness", "Conscientiousness", "Neuroticism", "Openness")

axis(1, at = seq(1.5, to = 9.5, by = 2), labels = bf.labels, line = 1, tick = FALSE)

dev.off()



# to do this, take behavioral ratings (set1, set2) from function above and see whether someting changes.




# correlation of mood and situational perception ---------------------
# lab.dia.panas <- cbind(dia.lab.data, panas.lab)
# 
# library(horst)
# library(Hmisc)
# getTable(lab.dia.panas[grep(x = names(lab.dia.panas), pattern = "VP+.+SET2")])


## positiven mit den negativen vergleichen.

# dazu, in dem set2.
# auf  TL_LAB_END_OTHER_LASTVID
# 1 = positiv
# 2 = negativ


## dont trust this function need to check this again. 
# select.sets <- function(data, var){
#   neg.set2.ll <- data$TL_LAB_END_OTHER_LASTVID == 2
#   
#   var.set1 <- var
#   var.set2 <- gsub("1", "2", var)
#   
#   neg.set1 <- data[(!neg.set2.ll), var.set1] # from set 1, we take the ones that have seen a positive in set 2, i.e a negative
#   neg.set2 <- data[(neg.set2.ll), var.set2] #
#   
#   pos.set1 <- data[(neg.set2.ll), var.set1] # from set 1, take the ones that saw a negative in set 2
#   pos.set2 <- data[(!neg.set2.ll), var.set2] # take the ones that saw a positive in set 2
#   boxplot(c(neg.set1, neg.set2), c(pos.set1, pos.set2))
#   cor(c(neg.set1, neg.set2), c(pos.set1, pos.set2), use = "pairwise.complete")
# }


## check what the actual panasst is really



# correlation between measures experience sampling and measur --------


## de sets sind falsch, man muss einfach die sets miteinander vergleichen, nicht die personen raus nehmen. 

## check convergence of situational ratings based on the study

## presentation may be:

## first, show, that the experiment works, i. e. we can observe behavior.
## second: show that there are reliable differences in situational perception (should be a correlation between them?)
# third: look for mood effects, there are none

## correlates with behavior: self rated and other rated (Yasmin)
## stability of measures across the situation;
## effects of mood (none)
## manipulation check












